# FlappyBirb

A barebone Flappybird clone made only with SFML and standard library C++11

Used qt Creater as IDE and the embedded QMake

![alt text](https://gitlab.com/tobias1012/flappybirb/raw/0d14b827c6f1b721b18a1afab0c836492a2e8461/images/Game.png "The game")
the red blocks are the pipes you have to avoid and the green rectangle is the player, you can jump by pressing space or left clicking
