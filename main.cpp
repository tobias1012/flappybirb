#include <iostream>
#include <deque>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/Audio.hpp"
#include "SFML/System.hpp"

const int screenHeight = 720;
const int screenWidth = 1280;
const int maxHeight = 200;
/*
sf::Font font;
sf::Text score;
uint point = 0;
*/

class Pipe {
public:
    Pipe();
    sf::RectangleShape downPipe;
    sf::RectangleShape upPipe;
private:
    int height;
};

Pipe::Pipe(){
    this->height = (std::rand() % (screenHeight - maxHeight));
    downPipe.setSize(sf::Vector2f(30, height));
    upPipe.setSize(sf::Vector2f(30, screenHeight-height-maxHeight));
    downPipe.setFillColor(sf::Color::Red);
    upPipe.setFillColor(sf::Color::Red);
    downPipe.setPosition(screenWidth,screenHeight-height);
    upPipe.setPosition(screenWidth,0);

}

//The birb
class Player {
public:
    sf::RectangleShape sprite;
    Player();

    //checks for collision with pipes
    bool isAlive(sf::RectangleShape& sprite, std::deque<Pipe>& pipe)
    {
    for(uint i = 0; i < pipe.size(); i++)
        {
            if(sprite.getGlobalBounds().intersects(pipe[i].downPipe.getGlobalBounds()))
                return false;

            if(sprite.getGlobalBounds().intersects(pipe[i].upPipe.getGlobalBounds()))
                return false;
            //-20 for forgiving nature
            if(sprite.getPosition().y > screenHeight+20 || sprite.getPosition().y < -20)
                     return false;
        }
        return true;
    }

private:
    int height = 0;
};

Player::Player()
{
    sprite.setSize((sf::Vector2f(10,10)));
    sprite.setPosition(200,200);
}


void spawnPipe(std::deque<Pipe>& pipes)
{
    Pipe pipe;
    pipes.push_back(pipe);
    //point++;

}

class Game{
public:
    std::deque<Pipe> pipes;
    sf::RenderWindow window;
    sf::Clock clock;
    sf::Time t = clock.getElapsedTime();

    void draw(Player& player )
    {
        //score.setString("Score:" + point);
        window.clear();
        sf::Time dt = clock.getElapsedTime()-t;
        player.sprite.move(0,0.35);
        pipeMove();
        window.draw(player.sprite);
        //window.draw(score);
        if(dt > sf::milliseconds(1500))
        {
            spawnPipe(pipes);
            clock.restart();
        }
        for(uint i = 0; i < pipes.size();++i)
        {
            window.draw(pipes[i].downPipe);
            window.draw(pipes[i].upPipe);
        }

    }
    void gameLoop()
    {
        Player local;
        /*
        score.setFont(font);
        score.setPosition(sf::Vector2f(screenWidth-100,screenHeight-100));
        score.setFillColor(sf::Color::Green);
        score.setCharacterSize(24);*/
        local.sprite.setFillColor(sf::Color::Green);
        window.create(sf::VideoMode(screenWidth, screenHeight), "SFML");
        while(window.isOpen())
        {
            window.setMouseCursorVisible(false);
            while(local.isAlive(local.sprite, pipes))
            {
                sf::Event event;
                while (window.pollEvent(event))
                {
                    // "close requested" event: we close the window
                    if (event.type == sf::Event::Closed)
                    {
                        window.close();
                        break;
                    }
                    else if(event.type == sf::Event::KeyReleased || event.type == sf::Event::MouseButtonReleased)
                    {
                        if(event.key.code == sf::Keyboard::Space || event.mouseButton.button == sf::Mouse::Left)
                        {
                                local.sprite.move(0,-100);;
                        }
                    }

                }
                if(!window.isOpen())
                {
                    break;
                }
                window.display();
                draw(local);
            }
            sf::sleep(sf::seconds(2));
            window.close();
        }
    }
private:
    void pipeMove()
    {
        if(pipes.size() > 0)
        {
            for(uint i = 0; i < pipes.size();++i)
            {
                const float speed = -0.5;
                pipes[i].downPipe.move(speed,0);
                pipes[i].upPipe.move(speed,0);
            }
            if(pipes[0].downPipe.getPosition().x < 0)
            {
                pipes.pop_front();
            }
        }
    }
};

int main()
{

    Game game;
    game.gameLoop();
    return 0;
}
